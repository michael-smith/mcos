# minimal containerized onion service
These containers are used to host an onion service, where the hidden service provided through Tor is separated from the actual service. As described in the [official site](https://community.torproject.org/onion-services/setup/),
```
HiddenServiceDir /var/lib/tor/my-website/
HiddenServicePort 80 unix:/var/run/tor/my-website.sock
```
risks are invovled.

![All in one](Docs/images/all-in-one.jpg)

As soon as an attacker gains control over the hosted hidden service (e.g. a website) and manages to execute commands, it is simple to request the attackers server. The machine contacts their server over the plain internet, hence it is easy to log and therefore reveal the IP address of the machine. So the anonymity all depends on the security of the hosted service.

In contrast, there is whonix, that seperates the Tor network from the service through virtual machines and seperated networks. The containerized approach should fill the gap between an risky all-in-one and an awesome whonix setup, but more lightweight with less overhead and needed resources.

![Containerized](Docs/images/containerized.jpg)

When an attacker controls the hosted hidden service and manages to execute commands, they are run inside the container. The container is only in an internal network, that is connected to the reverse proxy. So this container has no access to the internet and therefore cannot contact the attackers server. In addition to the takeover, the attacker must also break out of the container or crawl their way through the reverse proxy (also no internet connection) and finally in the Tor container.

Further, the server side packages that are running the service are also not able to contact servers (no matter if clear or .onion) and therefore, when a package of the service (e.g. a website) gathers metadata/trackingdata or whatever, it has no chance to send this data to an analysis/tracking server.

# Quickstart
## Preconditions
`podman` and `make` must be installed. It would be better to use `podman-compose`, but unfortunately the current version 1.0.3 has some quirks when handling networks. `git` is also recommended.

For debian based systems, run
```
$ apt install podman make
```
Debian also needs in `/etc/containers/registries.conf` the entry
```
unqualified-search-registries = ["docker.io"]
```
Because podman containers run rootless, the container stops when the user logs out. To prevent this, enable linger with
```
loginctl enable-linger
```

## Starting the service
1. Clone this repo
    ```
    git clone https://gitlab.com/michael-smith/mcos.git
    ```
    or alternatively download the source as zip and unpack it.

1. Move into its directory
    ```
    cd mcos/
    ```

1. Build the Tor container
    ```
    make build
    ```

1. Start all containers
    ```
    make up
    ```

## Getting some information
1. To check if everything works,
    ```
    podman ps
    ```
    should report three running containers: `mcos_service`, `mcos_reverse-proxy` and `mcos_tor`.

1. The onion address for this service can be checked with
    ```
    cat bind-mount/log/service/hostname
    ```
    When Tor shouldn't create a new address and an existing should be used, stop the containers, remove the file `hostname`, `hs_ed25519_public_key` and `hs_ed25519_secret_key` and copy at least your secret key file to this directory.

1. Nyx shows some information about connections, bandwidth, ...
    ```
    podman exec -it mcos_tor nyx
    ```

1. Vnstat shows the amount of the traffic so far
    ```
    podman exec mcos_tor vnstat
    ```

## Container as systemd service
When systemd should handle the containers, so that they automatically start after boot, execute
```
make service
```
while the containers are up. This will create the services, stop the containers, reset volumes, enable and start all containers as services again. To start/stop or enable/disable all containers, there is `make service_start`, `make service_stop`, `make service_enable` and `make service_disable`.

For individual containers, use `systemctl --user <command> container-<container name>.service`.

## Stopping the containers
To stop the containers, either run `make down` or `make downv` [1], where the latter also removes the created volumes. That shouldn't be much of an issue, because the volumes are just the caches for Tor, nyx and vnstat. It is practical to keep them, for example vnstat will then loose the traffic counter.
```
podman ps -a
```
shouldn't report mcos_* containers anymore.

[1] Attention: Do not use these commands when the systemd services are active, because systemd doesn't create networks and volumes at start again. Use `make service_stop` or `systemctl --user stop <service>` instead.

# Replacing the hello world service
To get a useful service, simply replace the first container in `up` in the `Makefile` as desired. When the `Makefile` should be used in future, ensure that podman starts the container with `--name mcos_service`. `--dns none` and `--no-hosts` are highly recommended, otherwise podman will use the `/etc/resolv.conf` and `/etc/hosts` of the host machine, where the chances are high to find the IP address of the machine. The volumes (either bind-mounts or podman managed volumes) can and should be modified as desired for the new service.

When the hidden service is using another port than 80, also adjust `server 10.3.0.3:80;` in the [config of the reverse proxy](bind-mount/reverse-proxy/nginx.conf) and the `HiddenServicePort` in [torrc](bind-mount/tor/config/torrc).

Besides the service container, its depending containers (like databases) can also be placed in the same network `mcos_internal` with their individually assigned IP addresses and container names. Don't forget to modify `down` in the `Makefile` to stop the other containers, too and also `downv` for eventually newly created volumes.

A disadvantage of `--dns none` is that containers cannot reach each other in the same network with their name. So instead of connecting the frontend with the backend (e.g. website and its database) over their container names, use the IP addresses.

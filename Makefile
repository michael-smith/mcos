CURDIR=$(shell pwd)

build:
	podman build -t mcos_tor $(CURDIR)/tor

volume:
	-podman volume create mcos_tor_tor
	-podman volume create mcos_tor_nyx
	-podman volume create mcos_tor_vnstat

network:
	podman network create --internal --subnet 10.3.0.0/16 --gateway 10.3.0.1 mcos_internal
	podman network create --internal --subnet 10.2.0.0/16 --gateway 10.2.0.1 mcos_int_reverse-proxy
	podman network create --subnet 10.1.0.0/16 --gateway 10.1.0.1 mcos_external

up: volume network
	mkdir -pv $(CURDIR)/bind-mount/log/service
	mkdir -pv $(CURDIR)/bind-mount/log/reverse-proxy
	mkdir -pv $(CURDIR)/bind-mount/log/tor
	mkdir -pv -m 700 $(CURDIR)/bind-mount/tor/service

	podman run --name mcos_service \
	           --detach \
	           --rm \
	           --dns none \
	           --no-hosts \
	           --hostname three \
	           --network mcos_internal:ip=10.3.0.3 \
	           --volume $(CURDIR)/bind-mount/service/config/default.conf:/etc/nginx/conf.d/default.conf:Z,ro \
	           --volume $(CURDIR)/bind-mount/log/service:/var/log/nginx:Z \
	           --volume $(CURDIR)/bind-mount/service/www:/www:Z,ro \
	           nginx:mainline-alpine

	podman run --name mcos_reverse-proxy \
	           --detach \
	           --rm \
	           --dns none \
	           --no-hosts \
	           --hostname two \
	           --network mcos_internal:ip=10.3.0.2 \
	           --network mcos_int_reverse-proxy:ip=10.2.0.3 \
	           --volume $(CURDIR)/bind-mount/reverse-proxy/nginx.conf:/etc/nginx/nginx.conf:Z,ro \
	           --volume $(CURDIR)/bind-mount/log/reverse-proxy:/var/log/nginx:Z \
	           nginx:mainline-bookworm

	podman run --name mcos_tor \
	           --detach \
	           --rm \
	           --dns 1.1.1.1 \
	           --no-hosts \
	           --hostname one \
	           --network mcos_int_reverse-proxy:ip=10.2.0.2 \
	           --network mcos_external:ip=10.1.0.2 \
	           --volume $(CURDIR)/bind-mount/tor/config/torrc:/etc/tor/torrc:Z,ro \
	           --volume $(CURDIR)/bind-mount/tor/config/nyx:/root/.nyx/config:Z,ro \
	           --volume $(CURDIR)/bind-mount/log/tor:/var/log/tor:Z \
	           --volume $(CURDIR)/bind-mount/tor/service:/var/lib/tor/service:Z \
	           --volume mcos_tor_tor:/root/tor:Z \
	           --volume mcos_tor_nyx:/root/nyx:Z \
	           --volume mcos_tor_vnstat:/var/lib/vnstat:Z \
	           mcos_tor

down:
	-podman stop mcos_service
	-podman stop mcos_reverse-proxy
	-podman stop mcos_tor

	-podman network remove mcos_internal
	-podman network remove mcos_int_reverse-proxy
	-podman network remove mcos_external

downv: down
	-podman volume remove mcos_tor_tor
	-podman volume remove mcos_tor_nyx
	-podman volume remove mcos_tor_vnstat

service_create:
	podman generate systemd --new --name mcos_service -f
	podman generate systemd --new --name mcos_reverse-proxy -f
	podman generate systemd --new --name mcos_tor -f

	mkdir -vp ~/.local/share/systemd/user/
	mv -v container-*.service ~/.local/share/systemd/user/
	systemctl --user daemon-reload

service_enable:
	systemctl --user enable container-mcos_service.service
	systemctl --user enable container-mcos_reverse-proxy.service
	systemctl --user enable container-mcos_tor.service

service_disable:
	systemctl --user disable container-mcos_service.service
	systemctl --user disable container-mcos_reverse-proxy.service
	systemctl --user disable container-mcos_tor.service

service_start:
	systemctl --user start container-mcos_service.service
	systemctl --user start container-mcos_reverse-proxy.service
	systemctl --user start container-mcos_tor.service

service_stop:
	systemctl --user stop container-mcos_service.service
	systemctl --user stop container-mcos_reverse-proxy.service
	systemctl --user stop container-mcos_tor.service

service: service_create downv volume network service_enable service_start
